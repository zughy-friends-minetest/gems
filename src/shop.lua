local S = core.get_translator("gems_battle")

local make_desc = function(gem_name, cost,count,longdesc,currency)
    
    local str = core.colorize("#a93b3b","Pay:")..
    " "..
    cost.. " "..
    core.colorize(gems_battle.get_color_code(gem_name),currency).."\n"..
    core.colorize("#3c5956","Get:")..
    " "..
    count.. " ct.".."\n\n"..
    longdesc.."\n"

    return str
end

local get_currency = gems_battle.get_currency


gems_battle.prices = {}
gems_battle.prices.gems_stuff = {}
gems_battle.prices.reg_stuff = {}

for _,gem_name in pairs(gems_battle.team_names) do
    local capname = gems_battle.get_capname(gem_name)
    local colorcap = core.colorize(gems_battle.get_color_code(gem_name),capname)
    gems_battle.prices.gems_stuff[gem_name]={}
    gems_battle.prices.gems_stuff[gem_name]["gems_battle:block_"..gem_name]    = {5 , 1 ,S("A strong block that can only @\nbe broken with gem picks")}
    gems_battle.prices.gems_stuff[gem_name]["gems_battle:glass_"..gem_name]    = {3 , 1, S("A block that is strong @\nagainst normal tools")}
    gems_battle.prices.gems_stuff[gem_name]["gems_battle:pick_" .. gem_name]     = {15 , 1, S("A pick to break Great Gems. Works FASTEST against @1 Great Gems.", colorcap)}
    gems_battle.prices.gems_stuff[gem_name]["gems_battle:sword_" .. gem_name]    = {18 , 1, S("A Powerful Sword. Kills @1 players faster.", colorcap)}

    gems_battle.prices.gems_stuff[gem_name]["gems_battle:".. gem_name .. "_reveal"]     = {25, 1, S("Reveals the current location @\nof players on @1 team to all your team members for a short time.@\n Recharges slowly.", colorcap)}
    gems_battle.prices.gems_stuff[gem_name]["gems_battle:protector_" .. gem_name]    = {13, 1, S("Damages all players in a radius EXCEPT @1 team players.@\n Cannot be placed near other protectors.", colorcap)}
end

gems_battle.prices.reg_stuff["default:pick_steel"] = {3 , 1, S("A normal steel Pick. CANNOT break Great Gems!")}
gems_battle.prices.reg_stuff["default:sword_steel"]= {3 , 1, S("A normal steel Sword")}
gems_battle.prices.reg_stuff["default:axe_steel"]  = {3 , 1, S("A normal steel Axe")}
-- gems_battle.prices.reg_stuff[bronze_pick  ]= {2 , 1}
-- gems_battle.prices.reg_stuff[bronze_sword ]= {2 , 1}

-- gems_battle.prices.reg_stuff[bronze_axe   ]= {2 , 1}

gems_battle.prices.reg_stuff["default:stone"]       = {1 , 5, S("Stone - A good building material")}
gems_battle.prices.reg_stuff["default:obsidian"]    = {1 , 2, S("Obsidian - A tough block")}
gems_battle.prices.reg_stuff["default:wood"]        = {1 , 5, S("Wood - A good building material")}
gems_battle.prices.reg_stuff["default:tree"]        = {1 , 2, S("Tree - A tough block")}
gems_battle.prices.reg_stuff["default:glass"]       = {1 , 5, S("Glass - A good building material")}
gems_battle.prices.reg_stuff["default:obsidian_glass" ]= {1 , 3, S("Obsidian Glass - Can only be broken with a pick")}

gems_battle.prices.reg_stuff["default:apple"]       = {4 , 1, S("Heals one heart. Cooldowns apply")}

gems_battle.prices.reg_stuff["enderpearl:ender_pearl"]= {22, 1, S("Enderpearl - Teleports you when thrown")}
gems_battle.prices.reg_stuff["default:torch"]        = {3, 1, S("Torch - Light up those dark spaces")}

-- gems_battle.prices.gem_shield   = {18, 1}
gems_battle.prices.reg_stuff["gems_battle:helmet_steel" ]   = {5 , 1, S("Helmet - A good protection against damage")}
gems_battle.prices.reg_stuff["gems_battle:chestplate_steel"]= {6 , 1,S("Chestplate - A good protection against damage")}
gems_battle.prices.reg_stuff["gems_battle:leggings_steel" ] = {6 , 1,S("Leggings - A good protection against damage")}
gems_battle.prices.reg_stuff["gems_battle:boots_steel" ]    = {5 , 1,S("Boots - A good protection against damage")}



-- build our detached shop invs


for _,gem_name in pairs(gems_battle.team_names) do

    -- the string for plural of gem_name
    local currency = gems_battle.get_currency(gem_name)

    core.create_detached_inventory("shop_"..gem_name, {
        allow_move = function(inv, from_list, from_index, to_list, to_index, count, player) return 0 end,
        allow_put = function(inv, listname, index, stack, player) return 0 end,
        allow_take = function(inv, listname, index, stack, player)
            local p_inv = player:get_inventory()
            local s_meta = stack:get_meta()
            local s_cost = s_meta:get_int("cost")
            local coststack = ItemStack("gems_battle:"..gem_name)
            coststack:set_count(s_cost)
            local taken = p_inv:remove_item("main", coststack)

            if taken:get_count() == coststack:get_count() then
                local givestack = ItemStack(stack:get_name().." "..stack:get_count())
                p_inv:add_item("main",givestack)
                return 0
            else
                p_inv:add_item("main", taken)
                core.chat_send_player(player:get_player_name(),core.colorize("#f47e1b", S("[!] You don't have enough @1!",currency)))
                return 0
            end
        end,

    })

    local inv = core.get_inventory({type="detached", name="shop_"..gem_name})
    local newlist = {}
    for i=1,8*4 do
        table.insert(newlist,ItemStack(""))
    end

    inv_inc = 1
    for item,cost_data in pairs(gems_battle.prices.gems_stuff[gem_name]) do
        local cost = cost_data[1]
        local count = cost_data[2]
        local longdesc = cost_data[3]
       
        local entry = ItemStack(item.." "..count)
        local entrymeta = entry:get_meta()
        entrymeta:set_string("description", make_desc(gem_name,cost,count,longdesc,currency))
        entrymeta:set_int("cost",cost)
        newlist[inv_inc] = entry
        inv_inc = inv_inc + 1
    end
    for item,cost_data in pairs(gems_battle.prices.reg_stuff) do
        local cost = cost_data[1]
        local count = cost_data[2]
        local longdesc = cost_data[3]
        
        local entry = ItemStack(item.." "..count)
        local entrymeta = entry:get_meta()
        entrymeta:set_string("description", make_desc(gem_name,cost,count,longdesc,currency))
        entrymeta:set_int("cost",cost)
        newlist[inv_inc] = entry
        inv_inc = inv_inc + 1
    end


    inv:set_list("main", newlist)

end




function gems_battle.show_shop_formspec(team, player)

    local currency = gems_battle.get_currency(team)
    local colorcurrency = core.colorize(gems_battle.get_color_code(team), currency)
    local str1 = S("Hi! Bring me @1!",colorcurrency)
    local str2 = S("I will sell you my wares!")
    local str3 = S("Just take whatever you like!")
    local fs = {
        "formspec_version[4]",
        "size[10.75,15]",
        "no_prepend[]",
        "bgcolor[#FFFFFF88;true;#FFFFFF88]",
        "background[0,0;10.75,15;gems_shop_bg.png;false]",
        "model[.5,.3;1.8,2.8;keeper;character.b3d;shopkeeper_"..team..".png;0,-150;false;false;x=0,y=0;0]",
        "label[4.7,1.1;"..str1.."]",
        "label[4.7,1.8;"..str2.."]",
        "label[4.7,2.5;"..str3.."]",
        "list[detached:shop_"..team..";main;.5,3.75;8,4;]",
        "list[current_player;main;.5,9.75;8,4;]",
        "listring[]"
    }

    core.show_formspec(player:get_player_name(), "shop_"..team, table.concat(fs))

end
